#!/usr/bin/env python

import sys, time, hashlib, shutil, logging, setproctitle, argparse, os
import config as conf
from daemon import Daemon


class Watcher(Daemon):

    def __init__(self, pidfile, logfile):
        super().__init__(pidfile)
        self.createLogger(logfile)



    def createLogger(self, logfile):
        self.logger = logging.getLogger('watcher')
        hdlr = logging.FileHandler(logfile)
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        hdlr.setFormatter(formatter)
        self.logger.addHandler(hdlr)
        self.logger.setLevel(logging.INFO)



    def hashFile(self, fileName):
        h = hashlib.md5()
        with open(fileName, 'rb') as f:
            buf = f.read()
            h.update(buf)
        return h.hexdigest()


    def run(self):

        setproctitle.setproctitle('watcher')
        hashes = {}
        for key in conf.files.keys():

            hashes[key] = self.hashFile(key)
            if hashes[key] != self.hashFile(conf.files[key]):
                self.logger.info("File " + key + " copied in file" + conf.files[key])
                shutil.copy(key, conf.files[key])

        while True:
            time.sleep(1)
            for key in hashes.keys():
                h = self.hashFile(key)
                if hashes[key] != h:
                    hashes[key] = h
                    shutil.copy(key, conf.files[key])
                    self.logger.info("File " + key + " copied in file " + conf.files[key])

def createParser():
    parser = argparse.ArgumentParser()
    parser.add_argument ('command', choices=['start', 'stop', 'restart'], help='')
    parser.add_argument ('-l', '--logfile', type=str, help='set an alternate log file')

    return parser

def configurationAudit(namespace):
    try:
        getattr(conf, "files")
    except AttributeError:
        print("Incorrect configuration. Variable \"files\" is not defined.")
        sys.exit(1)

    if type(conf.files) != type(dict()):
        print("Incorrect configuration. Variable \"files\" is not dict.")
        sys.exit(1)

    for key in conf.files.keys():
        if not os.access(key, os.F_OK):
            print("No such file: \"" + key + "\"")
            sys.exit(1)

        if not os.access(key, os.R_OK):
            print("File \"" + key + "\" cannot be read")
            sys.exit(1)

        if not os.access(conf.files[key], os.F_OK):
            print("No such file: \"" + conf.files[key] + "\"")
            sys.exit(1)

        if not os.access(conf.files[key], os.W_OK):
            print("File \"" + conf.files[key] + "\" cannot be written")
            sys.exit(1)

    if not len(conf.files):
        print("There isn't pairs of files.")
        sys.exit(1)

    if namespace.command == "stop":
        return

    if not namespace.logfile:
        try:
            getattr(conf, "logfile")
        except AttributeError:
            print("Specify file for logging.")
            sys.exit(2)

        if type(conf.logfile) != type(str()):
            print("Incorrect configuration. Variable \"logfile\" is not str.")
            sys.exit(1)
    else:
        conf.logfile = namespace.logfile


    if not os.access(conf.logfile, os.F_OK):
        print("No such file: \"" + conf.logfile + "\"")
        sys.exit(1)

    if not os.access(conf.logfile, os.W_OK):
        print("File \"" + conf.logfile + "\" cannot be written")
        sys.exit(1)

    return



if __name__ == "__main__":
    pidfile = '/tmp/daemon-warcher.pid'
    parser = createParser()

    namespace = parser.parse_args(sys.argv[1:])
    configurationAudit(namespace)

    daemon = Watcher(pidfile, conf.logfile)

    if namespace.command == 'start':
        print("Starting watcher")
        daemon.start()
    elif namespace.command == 'stop':
        print("Stoping watcher")
        daemon.stop()
    else:
        print("Restarting watcher")
        daemon.restart()
